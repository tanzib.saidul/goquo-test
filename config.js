const config = {
  local: {
    port: 3000,
    apiDomain: 'http://localhost:3000/api',
  }
};
module.exports = config.local;