'use strict';
const express = require('express');
const router = express.Router();
const unirest = require('unirest');
const flightService = require('../services/flightService');
const sanitizer = require('../sanitizer');
router.post('/flight/cheap-quotes', function (req, res) {
  const sanitizedData = sanitizer.getCheapestQuote(req.body);
  if (!sanitizedData) return res.sendStatus(400);
  flightService.getCheapestQuote(sanitizedData, quotes => {
    res.status(200).json(quotes);
  })
});

router.post('/flight/price-list', function (req, res) {
  const sanitizedData = sanitizer.getCheapestQuote(req.body);
  if (!sanitizedData) return res.sendStatus(400);
  flightService.getPriceList(sanitizedData, list => {
    res.status(200).json(list);
  })
});

module.exports = router;
