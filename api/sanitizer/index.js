'use strict';
module.exports = {
  getCheapestQuote:  body => {
    if (!body) return null;
    if (!body.outboundDate || !body.originPlace || !body.destinationPlace)  null;
    const _body = {
      originPlace: body.originPlace,
      destinationPlace: body.destinationPlace,
      outboundDate: body.outboundDate
    };
    return _body;
  }
};