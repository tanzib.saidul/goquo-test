const logger = require('logger').createLogger();
const moment = require('moment');
const apiKey = "d26df4ca0emshbc540d5f658ddf6p19dc18jsn62cec65ede42";
const unirest = require('unirest');

const getUpdatedList = async sessionId => {
  logger.info('Get updated price list');
  return await new Promise((resolve) => {
    const getList = () => {
      unirest.get("https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/pricing/uk2/v1.0/"+sessionId+"?sortType=price&sortOrder=asc&pageIndex=0&pageSize=10")
        .header("X-RapidAPI-Key", apiKey)
        .end(function (result) {
          logger.info('Updated price list response status: ', result.body.Status);
          if(result.body.Status === 'UpdatesComplete') {
            resolve(result.body);
          } else {
            getList();
          }
        });
    };
    getList();
  })
};

const groupFlightDetails = flightList => {
  const groupedList = [];
  for (let i=0; i<flightList.Itineraries.length; i++) {
    const data = {};
    const itinerarie = flightList.Itineraries[i];
    const outboundLegId = itinerarie.OutboundLegId;
    for (price of itinerarie.PricingOptions){
      data.Price = price.Price || 'N/A';
      const leg = flightList.Legs.find(element => element.Id === outboundLegId);
      data.Duration = leg.Duration ? (leg.Duration/60).toFixed(2) : 'N/A';
      data.Departure = leg.Departure || 'N/A';
      data.Arrival = leg.Arrival || 'N/A';
      const destination = flightList.Places.find(element => element.Id === leg.DestinationStation);
      data.destination = destination.Name +' '+ destination.Type;
      const origin = flightList.Places.find(element => element.Id === leg.OriginStation);
      data.origin = origin.Name +' '+ origin.Type;
      const carrier = flightList.Carriers.find(element => element.Id === leg.Carriers[0]);
      data.carrierName = carrier.Name || 'N/A';
      data.carrierLogo = carrier.ImageUrl || 'N/A';
      groupedList.push(data);
    }
  }
  return groupedList.sort((a, b) => parseFloat(a.Price) - parseFloat(b.Price));
};

module.exports = {
  getCheapestQuote: (body, cb) => {
    const results = [];
    const url = `https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browsequotes/v1.0/US/USD/en-US/${body.originPlace}/${body.destinationPlace}/`;
    for (let i=0; i<30;i++) {
      const date = moment(body.outboundDate, 'YYYY-MM-DD').add(i, 'days').format('YYYY-MM-DD');
      const endpoint = url+date;
      results.push(new Promise((resolve, reject) => {
        unirest.get(endpoint)
          .header("X-RapidAPI-Key", apiKey)
          .end(resposne => {
            if ( resposne.status === 200) {
              resolve({date, body: resposne.body})
            } else {
              resolve({date, body: {}})
            }
          })
      }))
    }
    Promise.all(results).then(data => {
      return cb(data);
    })
  },
  getPriceList: async (body, cb) => {
    const sessionId = await new Promise(resolve => {
      unirest.post("https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/pricing/v1.0")
        .header("X-RapidAPI-Key", apiKey)
        .header("Content-Type", "application/x-www-form-urlencoded")
        .send("children=0")
        .send("infants=0")
        .send("groupPricing=false")
        .send("country=US")
        .send("currency=USD")
        .send("locale=en-US")
        .send(`originPlace=${body.originPlace}`)
        .send(`destinationPlace=${body.destinationPlace}`)
        .send(`outboundDate=${body.outboundDate}`)
        .send("adults=1")
        .end(function (result) {
          resolve(result.headers.location.substr(result.headers.location.lastIndexOf('/') + 1));
        });
    });
    const list = await new Promise((resolve) => {
      resolve(getUpdatedList(sessionId));
    });

    return cb(groupFlightDetails(list));
  },
};

