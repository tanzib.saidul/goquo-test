var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'public');
var APP_DIR = path.resolve(__dirname, 'src');

function getPlugins(){
  var plugins = [];

  plugins.push(new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                "window.jQuery": "jquery'",
                "window.$": "jquery"
              })
            )
  return plugins;
}

var config = {
  watch: true,
  cache: false,
  resolve: { 
    modules: [APP_DIR, BUILD_DIR,"node_modules"], 
    extensions: [".js", ".json", ".jsx"] 
  },
    entry: [APP_DIR + '/index.js'],
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js'
    },
    plugins: getPlugins(),
    module: {
      rules: [
        {
          test: /\.jsx?/,
          include: APP_DIR,
          exclude: /node_modules/,
          use: [{
            loader: 'babel-loader',
            options: {
              plugins: ['transform-object-rest-spread']
            }
          }]
        },
        {
          test: /vendor\/.+\.(jsx|js|ico)$/,
          use: ['imports-loader?jQuery=jquery,$=jquery,this=>window']
        },
        {
          test: /\.css$/,
          loader: "style-loader!css-loader?root=."
        },
        {
          test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
          use: "url-loader?limit=10000&mimetype=application/font-woff"
        }, {
          test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
          use: "url-loader?limit=10000&mimetype=application/font-woff"
        }, {
          test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
          use: "url-loader?limit=10000&mimetype=application/octet-stream"
        }, {
          test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
          use: "file-loader"
        }, {
          test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
          use: "url-loader?limit=10000&mimetype=image/svg+xml"
        }
      ]
    }
};

module.exports = config;
