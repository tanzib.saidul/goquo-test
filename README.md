## Test project for GoQuo fullstack developer position
Need to display flight fares for 30 days from selected date.

## Candidate
* Name: Saidul Islam Buiyan
* Email: tanzibbdc@gmail.com
* Phone: 0176459026

## How to run and install
1) install node 10 (https://nodejs.org/en/).
2) install nodemon. 'npm install -g nodemon'
3) on project directory run 'npm install'
4) on project directory run 'npm run build'
5) close webpack (ctrl + c or cmd + c in macOs)
5) on project directory run 'npm run start'

The app will run on port 3000

## Tech/Architecture Explanation

This is a react/Node.js monolithic app.
The Frontend services calling flight api endpoint in node.js server (same domain).
The node.js server doing all the api calls to the rapidapi/skyscanner flight services.
Its also handling all the heady load of calling multiple/recursive api, grouping and sorting data.

## User journey

The user need to select departure date and route. By deafult its
the current date and KUL-SIN route.
After clicking search it will show a slider calender of 30 days from selected date and available flights in selected date.
The slider content also contain price that starts in that day from cheapest quotes api.
User can click this date to get all the available flights in that day.

## Note
* From Browse Quotes Endpoint not everyday data is available.
* The data from Poll live flight search endpoint takes a little longer for that 'UpdatesComplete' status to happen.
* Not doing any pagination or lazy loading, just showing first 10 itineraries.