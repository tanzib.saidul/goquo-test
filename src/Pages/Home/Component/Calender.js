import React, { Component } from 'react';
import Slider from "react-slick";

class Calender extends Component {

  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
      settings: {
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 5
      }
    };
  }
  render() {
    return (
      <div className="row justify-content-center margin-top2-20">
        <div className="col-10">
          <Slider {...this.state.settings}>
            {this.props.quotes.map((day,index) => {
              let text = 'N/A';
              if (day.body.Quotes) {
                if(day.body.Quotes[0]) {
                  text = <div className="price-start">Price starts from ${day.body.Quotes[0].MinPrice}</div>
                }
              }
              return (
                <div
                  className={`${this.state.activeIndex === index ? 'active ': ''}day text-center`}
                  onClick={() => {
                    this.props.getFlightList(day.date);
                    this.setState({activeIndex: index});
                  }}
                  key={day.date}
                >
                  <div className="primary-text-color">{day.date}</div>
                  {text}
                  <div className="font-12">click to view more</div>
                </div>
              )
            })}
          </Slider>

        </div>
      </div>
    );
  }
}

export default Calender;
