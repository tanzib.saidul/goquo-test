import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Dropdown from 'react-dropdown'
import 'react-dropdown/style.css';
const constants = require('../../../Common/Constants');
import moment from 'moment';

class SearchOption extends Component {

  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date(),
      routes: [
        { label: 'Singapore - Kuala Lumpur', value: 'SIN_KUL' },
        { label: 'Kuala Lumpur - Singapore', value: 'KUL_SIN' },
        { label: 'Kuala Lumpur - San Francisco', value: 'KUL_SFO' }
      ],
      defaultRoute: { label: 'Kuala Lumpur - Singapore', value: 'KUL_SIN' },
    };
  }

  componentDidMount() {
    const initialData = {
      originPlace: constants.routes[this.state.defaultRoute.value][0],
      destinationPlace: constants.routes[this.state.defaultRoute.value][1],
      outboundDate: moment().format('YYYY-MM-DD'),
    };
    this.props.setInitialState(initialData);
  }

  onDateSelect = date => {
    this.setState({
      startDate: date
    });
    this.props.setOutboundDate(moment(date).format('YYYY-MM-DD'))
  };

  onRouteSelect = route => {
    const places = {
      originPlace: constants.routes[route.value][0],
      destinationPlace: constants.routes[route.value][1],
    };
    this.setState({
      defaultRoute: route
    });
    this.props.setPlace(places);
  };
  render() {
    return (
      <div className="col-10">
        <div className="row justify-content-center">
          <div className="col-5 departure">
            <div><label>Select Departure Date</label></div>
            <DatePicker
              selected={this.state.startDate}
              onChange={this.onDateSelect}
              minDate={this.state.startDate}
            />
          </div>
          <div className="col-5 route">
            <div><label>Select Route</label></div>
            <Dropdown options={this.state.routes} value={this.state.defaultRoute} onChange={this.onRouteSelect} placeholder="Select an option" />
          </div>
        </div>
      </div>
    );
  }
}

export default SearchOption;
