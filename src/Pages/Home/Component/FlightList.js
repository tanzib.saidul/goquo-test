import React, { Component } from 'react';
import moment from 'moment';
class FlightList extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  renderCard = (flight,index) => {
    return (
      <div className="flight-card row justify-content-center margin-top2-20" key={index}>
        <div className="col-2">
          <img src={flight.carrierLogo} height="50" width="80"/>
          <h4>{flight.carrierName}</h4>
        </div>
        <div className="col-2">
          <div className="time">{moment(flight.Departure).format("h:mm:ss a")}</div>
          <div className="time">{moment(flight.Departure).format("MMM Do YYYY")}</div>
          <h4 className="place">{flight.origin}</h4>
        </div>
        <div className="col-2">
          <div className="time">{moment(flight.Arrival).format("h:mm:ss a")}</div>
          <div className="time">{moment(flight.Arrival).format("MMM Do YYYY")}</div>
          <h4 className="place">{flight.destination}</h4>
        </div>
        <div className="col-2">
          <div>{flight.Duration} hr</div>
        </div>
        <div className="col-2">
          <div className="price">${flight.Price}</div>
        </div>
      </div>
    )
  };

  render() {
    return (
      <div className="row justify-content-center margin-top2-20">
        <div className="col-10 text-left">
          <h4>Showing {this.props.flightList.length} items in ascending order.</h4>
        </div>
        <div className="col-10">
          {this.props.flightList.map((flight, index) => {
            return this.renderCard(flight, index)
          })}
        </div>
      </div>
    );
  }
}

export default FlightList;
