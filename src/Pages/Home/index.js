import React, { Component } from 'react';
import Header from "../../Common/Component/Header";
import SearchOption from "./Component/SearchOption";
import FlightService from '../../Services/flightService';
import Calender from './Component/Calender';
import FlightList from './Component/FlightList';

class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      originPlace: '',
      destinationPlace: '',
      outboundDate: '',
      loading: false,
      quotes: [],
      flightList: [],
    };
  }

  setPlace = places => {
    this.setState({
      originPlace: places.originPlace,
      destinationPlace: places.destinationPlace,
      quotes: [],
      flightList: [],
    })
  };

  setOutboundDate = date => {
    this.setState({
      outboundDate: date,
      quotes: [],
      flightList: [],
    })
  };

  // this method to render the dom only single time.
  setInitialState = body => {
    this.setState({
      originPlace: body.originPlace,
      destinationPlace: body.destinationPlace,
      outboundDate: body.outboundDate,
    });
  };

  search = async () => {
    this.setState({loading: true});
    const body = {
      originPlace: this.state.originPlace,
      destinationPlace: this.state.destinationPlace,
      outboundDate: this.state.outboundDate
    };
    try {
      const quotes = await FlightService.getCheapestQuotes(body);
      this.setState({quotes});
      const flightList = await FlightService.getFlightList(body);
      this.setState({loading: false, flightList});
    } catch (e) {
      this.setState({loading: false, quotes: [], flightList: []});
    }
  };

  getFlightList = async outboundDate => {
    this.setState({loading: true, flightList: []});
    const body = {
      originPlace: this.state.originPlace,
      destinationPlace: this.state.destinationPlace,
      outboundDate: outboundDate
    };
    try {
      const flightList = await FlightService.getFlightList(body);
      console.log(flightList);
      this.setState({loading: false, flightList});
    } catch (e) {
      this.setState({loading: false, flightList: []});
    }

  };

  render() {
    return (
      <div>
        <Header/>
        {this.state.loading ? <img src="img/loading.gif" className="loader"/> : ''}
        <div className="container">
          <h3 className='text-center'>Check flight status</h3>
          <div className="row justify-content-center margin-top2-20">
            <SearchOption
              setPlace={this.setPlace}
              setOutboundDate={this.setOutboundDate}
              setInitialState={this.setInitialState}
            />
            <div className="col-2">
              <button type="button" className="btn btn-primary btn-lg search-btn" onClick={this.search}>Search</button>
            </div>
          </div>
          {this.state.quotes.length > 0 ? <Calender quotes={this.state.quotes} getFlightList={this.getFlightList}/> : '' }
          {this.state.flightList.length > 0 ? <FlightList flightList={this.state.flightList} /> : ''}
        </div>
      </div>
    );
  }
}

export default Home;
