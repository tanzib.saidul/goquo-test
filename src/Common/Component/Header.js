import React, { Component } from 'react';

class Header extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="font-10">
        <h3 className="font-10">GoQuo full stack developer assignment.</h3>
      </div>
    );
  }
}

export default Header;
