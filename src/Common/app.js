import React, { Component } from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import Home from '../Pages/Home';

class App extends Component {
  render() {
    return (
      <Switch>        
        <Route path="/" exact component={Home} />
      </Switch>
    );
  }
}

export default App;
