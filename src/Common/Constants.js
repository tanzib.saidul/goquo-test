const routes = {
  SIN_KUL: ['SIN-sky', 'KUL-sky'],
  KUL_SIN: ['KUL-sky', 'SIN-sky'],
  KUL_SFO: ['KUL-sky', 'SFO-sky'],
};

export {
  routes
};