const axios = require('axios');
import config from '../../config';

export default class FlightService {
  /**
   * Get cheapest flight quotes
   * @param {Object} body
   * @property {String} originPlace
   * @property {String} destinationPlace
   * @property {String} outboundDate
   * @returns {Promise<Array>}
   */
  static getCheapestQuotes(body) {
    return axios.post(`${config.apiDomain}/flight/cheap-quotes`, body).then(response => response.data);
  }

  /**
   * Get list of flight
   * @param {Object} body
   * @property {String} originPlace
   * @property {String} destinationPlace
   * @property {String} outboundDate
   * @returns {Promise<Array>}
   */
  static getFlightList(body) {
    return axios.post(`${config.apiDomain}/flight/price-list`, body).then(response => response.data);
  }
}