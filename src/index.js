import React from 'react';
import  ReactDOM  from 'react-dom';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import App from './Common/app.js';

ReactDOM.render(
    <Router>        
      <App/>
    </Router>,
    document.getElementById('goQuo-app')
);
