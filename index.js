const config = require('./config');
const express = require('express');
const path = require('path');
const port = process.env.PORT || config.port;
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
app.disable('x-powered-by');

app.use(express.static(__dirname + '/public'));

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

let flight = require('./api/routes/flight.js');

app.use('/api',flight);

app.get('*', function (request, response){
  response.sendFile(path.resolve(__dirname, 'public', 'index.html'))
})

const server = require('http').Server(app);
server.listen(port);
console.log("GoQuo test app started on port " + port);
